#ifndef _CHALO_ENGINE_PROGRAM_HPP
#define _CHALO_ENGINE_PROGRAM_HPP

#include "../chalo-engine/Application/ChaloProgram.hpp"
#include "../chalo-engine/Managers/StateManager.hpp"

class ChaloEngineProgram : public chalo::ChaloProgram
{
    public:
    ChaloEngineProgram( bool fullscreen = false );
    ~ChaloEngineProgram();

    void Run();

    private:
    chalo::StateManager m_stateManager;

    void Setup( bool fullscreen = false );
    void Teardown();
    void SetupKeybindings();
};

#endif
