#include "ChaloEngineProgram.hpp"

#include "../States/StartupState.hpp"
#include "../States/DemoPickinSticksState.hpp"
#include "../States/GameState.hpp"
#include "../States/LanguageSelectState.hpp"
#include "../States/OptionsState.hpp"
#include "../States/HelpState.hpp"
#include "../States/TestState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/LanguageManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"

#include <cstdlib>
#include <ctime>

ChaloEngineProgram::ChaloEngineProgram( bool fullscreen /* = false */ )
{
    Setup( fullscreen );
}

ChaloEngineProgram::~ChaloEngineProgram()
{
    Teardown();
}

void ChaloEngineProgram::Setup( bool fullscreen /* = false */ )
{
    std::string full = ( fullscreen ) ? "1" : "0";

    std::map<std::string, std::string> options = {
        std::pair<std::string, std::string>( "CONFIG_NAME", "config.chaloconfig" ),
        std::pair<std::string, std::string>( "TITLEBAR_TEXT", "Mupoa Bo Pu - To The Unknown" ),
        std::pair<std::string, std::string>( "WINDOW_WIDTH", "1280" ),
        std::pair<std::string, std::string>( "WINDOW_HEIGHT", "720" ),
        std::pair<std::string, std::string>( "MENU_PATH", "Contents/Menus/" ),
        std::pair<std::string, std::string>( "FULLSCREEN", "0" ),
        std::pair<std::string, std::string>( "LANGUAGE", "0" ),
        std::pair<std::string, std::string>( "LANGUAGE_MAIN_FONT", "0" ),
        std::pair<std::string, std::string>( "SOUND_VOLUME", "100" ),
        std::pair<std::string, std::string>( "MUSIC_VOLUME", "50" ),
        std::pair<std::string, std::string>( "USE_OPEN_DYSLEXIC", "0" ),
        std::pair<std::string, std::string>( "USE_SUBTITLES", "1" ),
        std::pair<std::string, std::string>( "USE_CAPTIONS", "0" ),
        std::pair<std::string, std::string>( "DEBUG", "1" )
    };

    srand( time( NULL ) );

    chalo::ChaloProgram::Setup( options );

    // Set up language manager
    chalo::LanguageManager::SetLanguageBasePath( "Content/Languages/" );

    // Set up config manager
    chalo::ConfigManager::Setup( "config.chaloconfig" );
    bool loadedConfig = chalo::ConfigManager::Load();

    if ( loadedConfig )
    {
        // Load language
        chalo::LanguageManager::AddLanguage(
            chalo::ConfigManager::Get( "LANGUAGE_MAIN" ),
            chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" )
        );

        chalo::LanguageManager::AddLanguage(
            chalo::ConfigManager::Get( "TARGET_LANGUAGE" ),
            chalo::ConfigManager::Get( "TARGET_SUGGESTED_FONT" )
        );
    }
    else
    {
        // Set defaults
        for ( auto& option : options )
        {
            chalo::ConfigManager::Set( option.first, option.second );
        }
        chalo::ConfigManager::Save();
    }

    chalo::ConfigManager::Set( "DEBUG", "0" );

    // Set up global assets
    chalo::FontManager::Add( "main", "Content/Fonts/NotoSans-Bold.ttf" );
    chalo::FontManager::Add( "main-od", "Content/Fonts/OpenDyslexic-Bold.otf" );
    chalo::FontManager::Add( "devanagari", "Content/Fonts/NotoSansDevanagari-Bold.ttf" );

    // Set up menu manager
    chalo::MenuManager::Setup( "Content/Menus/" );

    // Set up draw manager
    chalo::DrawManager::Setup();

    // Set up input manager
    chalo::InputManager::Setup();
    SetupKeybindings();

    // Set up states
    chalo::IState* languageState        = new LanguageSelectState;
    chalo::IState* startupState         = new StartupState;
    chalo::IState* optionsState         = new OptionsState;
    chalo::IState* helpState            = new HelpState;
    chalo::IState* testState            = new TestState;
    // Demos
//    chalo::IState* pickinSticksState    = new PickinSticksState;
    chalo::IState* gameState            = new GameState;

    m_stateManager.InitManager();
    m_stateManager.AddState( "languagestate", languageState );
    m_stateManager.AddState( "startupstate", startupState );
    m_stateManager.AddState( "optionsstate", optionsState );
    m_stateManager.AddState( "helpstate", helpState );
    m_stateManager.AddState( "gamestate", gameState );
    m_stateManager.AddState( "teststate", testState );

    if (    chalo::ConfigManager::Get( "LANGUAGE_MAIN" ) == "" || chalo::ConfigManager::Get( "LANGUAGE_MAIN" ) == "0" ||
            chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" ) == "" || chalo::ConfigManager::Get( "LANGUAGE_MAIN_FONT" ) == "0" )
    {
        m_stateManager.ChangeState( "languagestate" );
    }
    else
    {
//        m_stateManager.ChangeState( "gamestate" );
        m_stateManager.ChangeState( "teststate" );
    }
}

void ChaloEngineProgram::Teardown()
{
//    chalo::InputManager::Teardown();
//    chalo::DrawManager::Teardown();
//    chalo::MenuManager::Teardown();

    chalo::ChaloProgram::Teardown();
}

void ChaloEngineProgram::Run()
{
    while ( chalo::Application::IsRunning() )
    {
        chalo::Application::BeginDrawing();
        chalo::Application::Update();
        m_stateManager.UpdateState();

        if ( m_stateManager.GetGotoState() != "" )
        {
            m_stateManager.ChangeState( m_stateManager.GetGotoState() );
        }

        // Drawing
        m_stateManager.DrawState( chalo::Application::GetWindow() );
        chalo::Application::EndDrawing();
    }
}

void ChaloEngineProgram::SetupKeybindings()
{
    // Keybinding setup: Input action type, keyboard key bindings, joystick button bindings, joystick axis bindings
    chalo::InputManager::SetKeybindings( {
        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_NORTH ),
            { sf::Keyboard::W },
            {},
            { /*chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::Y, -100 )*/ } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_SOUTH ),
            { sf::Keyboard::S },
            {},
            { /*chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::Y, 100 )*/ } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_WEST ),
            { sf::Keyboard::A },
            {},
            { /*chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::X, -100 )*/ } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_EAST ),
            { sf::Keyboard::D },
            {},
            { /*chalo::JoystickAxisBinding( 0, sf::Joystick::Axis::X, 100 )*/ } ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ),
            { sf::Keyboard::Q },
            { chalo::JoystickButtonBinding( 0, 0 ), chalo::JoystickButtonBinding( 0, 2 ), chalo::JoystickButtonBinding( 0, 9 ) },
            {} ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION2 ),
            { sf::Keyboard::E },
            { chalo::JoystickButtonBinding( 0, 1 ), chalo::JoystickButtonBinding( 0, 3 ) },
            {} ),

        chalo::InputBinding( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION6 ),
            { sf::Keyboard::BackSpace },
            { chalo::JoystickButtonBinding( 0, 8 ) },
            {} )
    } );
}
