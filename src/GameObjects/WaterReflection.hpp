#ifndef _WaterReflection_HPP
#define _WaterReflection_HPP

#include "ProceduralObject.hpp"

class WaterReflection : public ProceduralObject
{
    public:
    WaterReflection();
    WaterReflection( sf::Vector2f pos );
    virtual ~WaterReflection() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
