#ifndef _JELLYFISH_HPP
#define _JELLYFISH_HPP

#include "ProceduralObject.hpp"

class Jellyfish : public ProceduralObject
{
    public:
    Jellyfish();
    Jellyfish( sf::Vector2f pos );
    virtual ~Jellyfish() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
