#include "Splotch.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

#include <cmath>

Splotch::Splotch()
{
    m_type = "Splotch";
}

Splotch::Splotch( sf::Vector2f pos )
{
    m_type = "Splotch";
    Setup( pos );
}

Splotch::Splotch( sf::Vector2f pos, sf::Color color )
{
    m_type = "Splotch";
    Setup( pos, color );
}

void Splotch::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "Splotch::Setup" );
    BuildObject( pos, 0, sf::Color( 59, 79, 144 ) );
    ProceduralObject::Setup();
}

void Splotch::Setup( sf::Vector2f pos, sf::Color color )
{
    ProceduralObject::Setup();
    BuildObject( pos, 0, color );
}

void Splotch::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float radius = chalo::GetRandomNumber( 50, 300 );
    float halfsies = radius * sqrt( 2 ) / 2;

    m_vertexArrays[0].setPrimitiveType( sf::TriangleFan );

    // Center vertex
    m_vertexArrays[0].append( sf::Vertex( pos, color ) );

    // Outside triangles
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x,                  pos.y ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + radius,         pos.y ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + halfsies,       pos.y + halfsies/2 ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x,                  pos.y + radius/2 ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - halfsies,       pos.y + halfsies/2 ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - radius,         pos.y ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - halfsies,       pos.y - halfsies/2 ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x,                  pos.y - radius/2 ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + halfsies,       pos.y - halfsies/2 ), color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + radius,         pos.y ), color ) );
}


