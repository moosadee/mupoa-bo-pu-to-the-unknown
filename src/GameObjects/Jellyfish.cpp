#include "Jellyfish.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"

Jellyfish::Jellyfish()
{
    m_type = "Jellyfish";
}

Jellyfish::Jellyfish( sf::Vector2f pos )
{
    m_type = "Jellyfish";
    Setup( pos );
}

void Jellyfish::Setup( sf::Vector2f pos )
{
    BuildObject( pos, 0, sf::Color( 222, 175, 52 ) );
    ProceduralObject::Setup();
}

void Jellyfish::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float h = chalo::GetRandomNumber( 30, 40 );
    float w = h * 3;

    // Body
    sf::Vector2f base1 = sf::Vector2f( pos.x, pos.y );
    sf::Vector2f base2 = sf::Vector2f( pos.x + w/3, pos.y );
    sf::Vector2f base3 = sf::Vector2f( (base1.x + base2.x) / 2, pos.y - h );

    AddVertex( 0, base1, color );
    AddVertex( 0, base2, color );
    AddVertex( 0, base3, color );


    sf::Vector2f base4 = base2;
    sf::Vector2f base5 = sf::Vector2f( base4.x + w/3, pos.y );
    sf::Vector2f base6 = sf::Vector2f( (base4.x + base5.x) / 2, pos.y - h );

    AddVertex( 0, base4, color );
    AddVertex( 0, base5, color );
    AddVertex( 0, base6, color );


    sf::Vector2f base7 = base3;
    sf::Vector2f base8 = base2;
    sf::Vector2f base9 = base6;

    AddVertex( 0, base7, color );
    AddVertex( 0, base8, color );
    AddVertex( 0, base9, color );

    // Tentacles
    float bottom = pos.y + h;
    float tentwidth = 10;
    float x = pos.x + tentwidth;
    for ( int i = 0; i < w / tentwidth - 6; i++ )
    {
        sf::Vector2f r1 = sf::Vector2f( x, pos.y );
        sf::Vector2f r2 = sf::Vector2f( x + tentwidth, pos.y );
        sf::Vector2f r3 = sf::Vector2f( x + tentwidth/2, pos.y + h );

        AddVertex( 0, r1, color );
        AddVertex( 0, r2, color );
        AddVertex( 0, r3, color );

        x += tentwidth;
    }
}


