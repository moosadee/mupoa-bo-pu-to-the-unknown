#ifndef _TREECORAL_HPP
#define _TREECORAL_HPP

#include "ProceduralObject.hpp"

class TreeCoral : public ProceduralObject
{
    public:
    TreeCoral();
    TreeCoral( sf::Vector2f pos );
    virtual ~TreeCoral() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();
    virtual void Clear();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color, int parentIndex = -1, int childIndex = -1 );
};

#endif
