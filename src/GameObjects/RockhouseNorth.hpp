#ifndef ROCKNorth_HPP
#define ROCKNorth_HPP

#include "ProceduralObject.hpp"

class RockhouseNorth : public ProceduralObject
{
    public:
    RockhouseNorth();
    RockhouseNorth( sf::Vector2f pos );
    virtual ~RockhouseNorth() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
