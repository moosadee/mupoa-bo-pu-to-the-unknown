#ifndef _ROCK_HPP
#define _ROCK_HPP

#include "ProceduralObject.hpp"

class Rock : public ProceduralObject
{
    public:
    Rock();
    Rock( sf::Vector2f pos );
    virtual ~Rock() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
