#include "Player.hpp"

#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/StringUtil.hpp"

Player::Player()
{
}

Player::Player( sf::Vector2f pos )
{
    Setup( pos );
}

Player::~Player()
{
}

void Player::Setup( sf::Vector2f pos )
{
    m_obj.Setup( pos );
    m_slowSpeed = 2;
    m_fastSpeed = 4;
}

void Player::Update()
{
    m_obj.Update();

    float xMovement = 0, yMovement = 0;

    float speed = m_fastSpeed;

    // "walking"?
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION2 ) ) )
    {
        speed = m_slowSpeed;
    }

    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_NORTH ) ) )
    {
        yMovement = -speed;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_SOUTH ) ) )
    {
        yMovement = speed;
    }

    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_WEST ) ) )
    {
        xMovement = -speed;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_EAST ) ) )
    {
        xMovement = speed;
    }

    if ( xMovement != 0 && yMovement != 0 )
    {
        // sqrt(2) / 2
        yMovement *= 0.71;
        xMovement *= 0.71;
    }

    m_obj.Move( xMovement, yMovement );
}

void Player::Draw( sf::RenderWindow& window )
{
    m_obj.Draw( window );
}

