#ifndef _Geyser_HPP
#define _Geyser_HPP

#include "ProceduralObject.hpp"

class Geyser : public ProceduralObject
{
    public:
    Geyser();
    Geyser( sf::Vector2f pos );
    virtual ~Geyser() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
