#ifndef _CEPHALOPOD_HPP
#define _CEPHALOPOD_HPP

#include "ProceduralObject.hpp"

class Cephalopod : public ProceduralObject
{
    public:
    Cephalopod();
    Cephalopod( sf::Vector2f pos );
    virtual ~Cephalopod() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );

    std::vector<int> m_tentacleIndices;
};

#endif
