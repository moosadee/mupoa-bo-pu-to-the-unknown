#include "RockhouseSouth.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

RockhouseSouth::RockhouseSouth()
{
    m_type = "RockhouseSouth";
}

RockhouseSouth::RockhouseSouth( sf::Vector2f pos )
{
    m_type = "RockhouseSouth";
    Setup( pos );
}

void RockhouseSouth::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "RockhouseSouth::Setup" );
    BuildObject( pos, 0, sf::Color( 50, 54, 65 ) );
    ProceduralObject::Setup();
}

void RockhouseSouth::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float radius = 100;
    float height = 100;
    float quarteries = radius / 4;

    m_vertexArrays[0].setPrimitiveType( sf::TriangleFan );

    // Center vertex
    m_vertexArrays[0].append( sf::Vertex( pos, color ) );                                                           // a

    // Additional shape
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - radius,         pos.y ), color ) );                 // b
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - quarteries,     pos.y - height ), color ) );        // c
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + quarteries,     pos.y - height ), color ) );        // d
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + radius,         pos.y ), color ) );                 // e

    // Door
    sf::Color doorColor = sf::Color( 25, 25, 25 );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - 80, pos.y ),   doorColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - 40, pos.y - 60 ),   doorColor ) );
}
