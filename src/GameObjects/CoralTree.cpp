#include "CoralTree.hpp"

#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/StringUtil.hpp"
#include "../chalo-engine/Utilities/HelperFunctions.hpp"

TreeCoral::TreeCoral()
{
    m_type = "TreeCoral";
}

TreeCoral::TreeCoral( sf::Vector2f pos )
{
    m_type = "TreeCoral";
    Setup( pos );
}

void TreeCoral::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "TreeCoral::Setup" );
    m_maxFrame = 20;
    m_currentFrame = 0;
    m_animateSpeed = 1;
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );
//    m_vertexArrays[1].setPrimitiveType( sf::Triangles );
    BuildObject( pos, 0, sf::Color( 90, 0, 50 ), -1 );
    chalo::Logger::OutHighlight( "Setup done", "TreeCoral::Setup" );
    ProceduralObject::Setup();
}

void TreeCoral::BuildObject( sf::Vector2f pos, int step, sf::Color color, int parentIndex, int childIndex )
{
    std::string prefix = " ";
    if ( step == 1 ) { prefix = "* "; }
    if ( step == 2 ) { prefix = "** "; }

    float alterMax = 20;
    float lengthInc = 10;
    float widthInc = 5;

    int colorInc = 50;//10;
    color.r = ( color.r + colorInc > 255 ) ? 255 : color.r + colorInc;
    color.g = ( color.g + colorInc > 255 ) ? 255 : color.g + colorInc;
    color.b = ( color.b + colorInc > 255 ) ? 255 : color.b + colorInc;
    int alter = chalo::GetRandomNumber( -alterMax, alterMax );
    int children = 3;// rand() % 3 + 2;

    // Tip needs to be the last vertex added to the vertex array.
    sf::Vector2f tip;
    int tipIndex;

    // Terminating case: Step is max number
    if ( step == 3 )
    {
        return;
    }
    else if ( step == 0 )
    {
        // Root stalk
        alter = chalo::GetRandomNumber( -alterMax/2, alterMax/2 );

        sf::Vector2f rootLeft = sf::Vector2f ( -widthInc + pos.x, 0 + pos.y );
        sf::Vector2f rootRight = sf::Vector2f( widthInc + pos.x, 0 + pos.y );
        tip = sf::Vector2f( 0 + pos.x + alter, -lengthInc + pos.y );

        AddVertex( 0, rootLeft,     color );
        AddVertex( 0, rootRight,    color );
        AddVertex( 0, tip,          color );

        tipIndex = m_vertexArrays[0].getVertexCount() - 1;
    }
    else
    {
        int offset = pos.x;
        int tipOffset = 0;
        if ( childIndex == 0 )
        {
            offset -= widthInc;
            tipOffset = -widthInc/2;
        }
        else if ( childIndex == 2 )
        {
            offset += widthInc;
            tipOffset = widthInc/2;
        }

        sf::Vector2f connection = sf::Vector2f( pos.x, pos.y );
        sf::Vector2f left  = sf::Vector2f( offset - 5, pos.y - lengthInc );
        tip = sf::Vector2f( offset + tipOffset, pos.y - lengthInc*2 );

        AddVertex( 0, connection,       color );
        AddVertex( 0, left,             color );
        AddVertex( 0, tip,              color );

        tipIndex = m_vertexArrays[0].getVertexCount() - 1;
        m_vertexConnections[tipIndex].baseIndex = tipIndex;
    }

    // Connect these vertices together to their branching point.
    if ( parentIndex != -1 )
    {
        VertexConnection vc;
        m_vertexConnections[parentIndex].baseIndex = parentIndex;
        m_vertexConnections[parentIndex].connectionIndices.push_back( tipIndex-2 );
    }

    // Recurse for children
    for ( int i = 0; i < children; i++ )
    {
        BuildObject( tip, step + 1, color, tipIndex, i );
    }
}


void TreeCoral::Update()
{
//    chalo::Logger::Out( "Update", "TreeCoral::Update" );
    IncrementFrame();

    if ( m_currentFrame == 0 )
    {
        for ( auto& v : m_vertexConnections )
        {
            float xChange = chalo::GetRandomNumber( -1, 1 );
            float yChange = chalo::GetRandomNumber( -1, 1 );

            AdjustVertex( 0, v.second.baseIndex, xChange, yChange );

            for ( auto& c : v.second.connectionIndices )
            {
                m_vertexArrays[0][c].position = m_vertexArrays[0][v.second.baseIndex].position;
            }
        }
    }
}


void TreeCoral::Clear()
{
    ProceduralObject::Clear();
}
