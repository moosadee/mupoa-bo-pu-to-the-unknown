#ifndef _GRASSCORAL_HPP
#define _GRASSCORAL_HPP

#include "ProceduralObject.hpp"

class GrassCoral : public ProceduralObject
{
    public:
    GrassCoral();
    GrassCoral( sf::Vector2f pos );
    virtual ~GrassCoral() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
