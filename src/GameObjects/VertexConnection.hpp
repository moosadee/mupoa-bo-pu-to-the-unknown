#ifndef _CONNECTED_VERTEX_HPP
#define _CONNECTED_VERTEX_HPP

#include <SFML/Graphics.hpp>

class VertexConnection
{
    public:
    VertexConnection()
    {
        baseIndex = -1;
    }

    int baseIndex;
    std::vector<int> connectionIndices;
};

#endif
