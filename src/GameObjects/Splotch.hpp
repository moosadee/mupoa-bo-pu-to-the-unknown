#ifndef _Splotch_HPP
#define _Splotch_HPP

#include "ProceduralObject.hpp"

class Splotch : public ProceduralObject
{
    public:
    Splotch();
    Splotch( sf::Vector2f pos );
    Splotch( sf::Vector2f pos, sf::Color color );
    virtual ~Splotch() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Setup( sf::Vector2f pos, sf::Color color );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
