#include "WaterReflection.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

WaterReflection::WaterReflection()
{
    m_type = "WaterReflection";
}

WaterReflection::WaterReflection( sf::Vector2f pos )
{
    m_type = "WaterReflection";
    Setup( pos );
}

void WaterReflection::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "WaterReflection::Setup" );
    m_maxFrame = 10;
    m_currentFrame = 0;
    m_animateSpeed = 1;
    BuildObject( pos, 0, sf::Color( 255, 255, 255, 20 ) );
    ProceduralObject::Setup();
}

void WaterReflection::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::LineStrip );

    sf::Vector2f v1, v2, v3;

    float screenWidth = 1280;
    float totalColumns = 5;
    float widthPerColumn = screenWidth / totalColumns;

    float screenHeight = 720;
    float totalSegments = 10;
    float heightPerSegment = screenHeight / totalSegments;

    sf::Vector2f position = sf::Vector2f( 0, 0 );

    for ( int i = 0; i < 100; i++ )
    {
        position = chalo::GetRandomCoordinate( -200, -200, 1280+200, 720+200 );
        m_vertexArrays[0].append( sf::Vertex( position, color ) );
    }

    for ( int i = 0; i < 100; i++ )
    {
        position = chalo::GetRandomCoordinate( -200, -200, 1280+200, 720+200 );
        m_vertexArrays[1].append( sf::Vertex( position, color ) );
    }
}

void WaterReflection::Update()
{
//    chalo::Logger::Out( "Update", "WaterReflection::Update" );
    IncrementFrame();

    if ( int(m_currentFrame) == 0 )
    {
        for ( int i = 0; i < m_vertexArrays[0].getVertexCount(); i++  )
        {
            m_vertexArrays[0][i].position.x += chalo::GetRandomNumber( -1, 1 );
            m_vertexArrays[0][i].position.y += chalo::GetRandomNumber( -1, 1 );
        }

        for ( int i = 0; i < m_vertexArrays[0].getVertexCount(); i++  )
        {
            m_vertexArrays[1][i].position.x += chalo::GetRandomNumber( -1, 1 );
            m_vertexArrays[1][i].position.y += chalo::GetRandomNumber( -1, 1 );
        }
    }
}

