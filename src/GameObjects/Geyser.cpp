#include "Geyser.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

#include <cmath>

Geyser::Geyser()
{
    m_type = "Geyser";
}

Geyser::Geyser( sf::Vector2f pos )
{
    m_type = "Geyser";
    Setup( pos );
}

void Geyser::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "Geyser::Setup" );
    BuildObject( pos, 0, sf::Color( 26, 69, 205 ) );
    ProceduralObject::Setup();
}

void Geyser::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float radius = 100;
    float halfsies = radius * sqrt( 2 ) / 2;

    m_vertexArrays[0].setPrimitiveType( sf::TriangleFan );

    // Center vertex
    m_vertexArrays[0].append( sf::Vertex( pos, sf::Color( 12, 44, 75 ) ) );

    // Rocks
    sf::Color rockColor = sf::Color( 50+25, 54+25, 65+50 );
    AddVertex( 0, sf::Vector2f( pos.x + 20,                  pos.y + 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x + radius + 20,         pos.y + 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x + halfsies + 20,       pos.y + halfsies/2 + 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x + 20,                  pos.y + radius/2 + 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x - halfsies - 20,       pos.y + halfsies/2 + 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x - radius - 20,         pos.y + 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x - halfsies - 20,       pos.y - halfsies/2  - 20), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x - 20,                  pos.y - radius/2 - 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x + halfsies + 20,       pos.y - halfsies/2 - 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x + radius + 20,         pos.y - 20 ), rockColor );
    AddVertex( 0, sf::Vector2f( pos.x + radius + 20,         pos.y + 20 ), rockColor );

    // Outside triangles
    AddVertex( 0, sf::Vector2f( pos.x,                  pos.y ), color );
    AddVertex( 0, sf::Vector2f( pos.x + radius,         pos.y ), color );
    AddVertex( 0, sf::Vector2f( pos.x + halfsies,       pos.y + halfsies/2 ), color );
    AddVertex( 0, sf::Vector2f( pos.x,                  pos.y + radius/2 ), color );
    AddVertex( 0, sf::Vector2f( pos.x - halfsies,       pos.y + halfsies/2 ), color );
    AddVertex( 0, sf::Vector2f( pos.x - radius,         pos.y ), color );
    AddVertex( 0, sf::Vector2f( pos.x - halfsies,       pos.y - halfsies/2 ), color );
    AddVertex( 0, sf::Vector2f( pos.x,                  pos.y - radius/2 ), color );
    AddVertex( 0, sf::Vector2f( pos.x + halfsies,       pos.y - halfsies/2 ), color );
    AddVertex( 0, sf::Vector2f( pos.x + radius,         pos.y ), color );
}


