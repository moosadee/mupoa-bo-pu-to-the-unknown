#ifndef _FLUFFCORAL_HPP
#define _FLUFFCORAL_HPP

#include "ProceduralObject.hpp"

class FluffCoral : public ProceduralObject
{
    public:
    FluffCoral();
    FluffCoral( sf::Vector2f pos );
    virtual ~FluffCoral() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
