#include "ProceduralObject.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/StringUtil.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"

#include <cstdlib>
#include <exception>


ProceduralObject::ProceduralObject()
{
    m_type = "ProceduralObject";
    m_boundingBox.left = -1;
    m_boundingBox.top = -1;
    m_boundingBox.width = -1;
    m_boundingBox.height = -1;
}

ProceduralObject::ProceduralObject( sf::Vector2f pos )
{
    m_type = "ProceduralObject";
    m_boundingBox.left = -1;
    m_boundingBox.top = -1;
    m_boundingBox.width = -1;
    m_boundingBox.height = -1;
}

ProceduralObject::ProceduralObject( sf::Vector2f pos, sf::Color color )
{
    m_type = "ProceduralObject";
    m_boundingBox.left = -1;
    m_boundingBox.top = -1;
    m_boundingBox.width = -1;
    m_boundingBox.height = -1;
}

void ProceduralObject::Draw( sf::RenderWindow& window )
{
    for ( auto& vertexArray : m_vertexArrays )
    {
        window.draw( vertexArray.second );
    }

    // Debug mode: Draw bounding box
    if ( chalo::ConfigManager::GetBool( "DEBUG" ) )
    {
        sf::VertexArray debugArray;
        debugArray.setPrimitiveType( sf::LineStrip );
        debugArray.append( sf::Vertex( sf::Vector2f( m_boundingBox.left, m_boundingBox.top ), sf::Color::Red ) );
        debugArray.append( sf::Vertex( sf::Vector2f( m_boundingBox.left + m_boundingBox.width, m_boundingBox.top ), sf::Color::Red ) );
        debugArray.append( sf::Vertex( sf::Vector2f( m_boundingBox.left + m_boundingBox.width, m_boundingBox.top + m_boundingBox.height ), sf::Color::Red ) );
        debugArray.append( sf::Vertex( sf::Vector2f( m_boundingBox.left, m_boundingBox.top + m_boundingBox.height ), sf::Color::Red ) );
        debugArray.append( sf::Vertex( sf::Vector2f( m_boundingBox.left, m_boundingBox.top ), sf::Color::Red ) );
        window.draw( debugArray );
    }
}

void ProceduralObject::Setup()
{
    // copy vertex array
    m_originalPositions = m_vertexArrays;

//    chalo::Logger::Out( "VERTEX ARRAYS", "ProceduralObject::Setup" );
//    for ( auto& va : m_vertexArrays )
//    {
//        chalo::Logger::Out( "Key: " + chalo::StringUtility::ToString( va.first ) + ", Total vertices: " + chalo::StringUtility::ToString( va.second.getVertexCount() ), "ProceduralObject::Setup" );
//    }
//
//    chalo::Logger::Out( "ORIGINAL POSITIONS", "ProceduralObject::Setup" );
//    for ( auto& va : m_originalPositions )
//    {
//        chalo::Logger::Out( "Key: " + chalo::StringUtility::ToString( va.first ) + ", Total vertices: " + chalo::StringUtility::ToString( va.second.getVertexCount() ), "ProceduralObject::Setup" );
//    }
}

std::string ProceduralObject::GetType()
{
    return m_type;
}

void ProceduralObject::Clear()
{
    for ( auto& vertexArray : m_vertexArrays )
    {
        vertexArray.second.clear();
    }

    m_vertexConnections.clear();
}

void ProceduralObject::Update()
{
}

sf::IntRect ProceduralObject::GetBoundingBox()
{
    return m_boundingBox;
}

void ProceduralObject::SetLeft( float value )
{
    if ( m_boundingBox.left == -1 || value < m_boundingBox.left )
    {
        m_boundingBox.left = value;
    }
}

void ProceduralObject::SetTop( float value )
{
    if ( m_boundingBox.top == -1 || value < m_boundingBox.top )
    {
        m_boundingBox.top = value;
    }
}

void ProceduralObject::SetWidth( float value )
{
    if ( m_boundingBox.width == -1 || value > m_boundingBox.width )
    {
        m_boundingBox.width = value;
    }
}

void ProceduralObject::SetHeight( float value )
{
    if ( m_boundingBox.height == -1 || value > m_boundingBox.height )
    {
        m_boundingBox.height = value;
    }
}

void ProceduralObject::SetWidth( float leftXPos, float rightXPos )
{
    float width = rightXPos - leftXPos;
    SetWidth( width );
}

void ProceduralObject::SetHeight( float topYPos, float bottomYPos )
{
    float height = bottomYPos - topYPos;
    SetHeight( height );
}

float ProceduralObject::GetLeft()
{
    return m_boundingBox.left;
}

float ProceduralObject::GetRight()
{
    return m_boundingBox.left + m_boundingBox.width;
}

float ProceduralObject::GetWidth()
{
    return m_boundingBox.width;
}

float ProceduralObject::GetTop()
{
    return m_boundingBox.top;
}

float ProceduralObject::GetBottom()
{
    return m_boundingBox.top + m_boundingBox.height;
}

float ProceduralObject::GetHeight()
{
    return m_boundingBox.height;
}

void ProceduralObject::IncrementFrame()
{
    m_currentFrame += m_animateSpeed;
    if ( m_currentFrame >= m_maxFrame )
    {
        m_currentFrame = 0;
    }
}

void ProceduralObject::Move( float x, float y )
{
    for ( auto& vertexArray : m_vertexArrays )
    {
        for ( unsigned int i = 0; i < vertexArray.second.getVertexCount(); i++ )
        {
            vertexArray.second[i].position.x += x;
            vertexArray.second[i].position.y += y;
        }
    }

    // Update bounding box
    m_boundingBox.left += x;
    m_boundingBox.top += y;
}

void ProceduralObject::AdjustVertex( int key, int index, int xAmt, int yAmt )
{
    // Check if key is in the m_originalPositions
    try
    {
        m_originalPositions.at( key );
    }
    catch( const std::out_of_range& ex )
    {
        chalo::Logger::Error( "No key \"" + chalo::StringUtility::ToString( key ) + "\" in m_originalPosition!", "ProceduralObject::AdjustVertex" );
        return;
    }

    // Adjust positions
    float updatedX = m_vertexArrays[key][index].position.x + xAmt;
    float updatedY = m_vertexArrays[key][index].position.y + yAmt;

    float minX = m_originalPositions[key][index].position.x - 2;
    float maxX = m_originalPositions[key][index].position.x + 2;

    float minY = m_originalPositions[key][index].position.y - 2;
    float maxY = m_originalPositions[key][index].position.y + 2;

    if ( minX <= updatedX && updatedX <= maxX )
    {
        m_vertexArrays[key][index].position.x += xAmt;
    }

    if ( minY <= updatedY && updatedY <= maxY )
    {
        m_vertexArrays[key][index].position.y += yAmt;
    }

}

void ProceduralObject::AddVertex( int arrayIndex, sf::Vector2f position, sf::Color color )
{
    m_vertexArrays[ arrayIndex ].append( sf::Vertex( position, color ) );

    // Check if this is min/max position for bounding box
    UpdateBoundingBox( position );
}

void ProceduralObject::UpdateBoundingBox( sf::Vector2f pos )
{
    SetLeft( pos.x );
    SetTop( pos.y );
    SetWidth( m_boundingBox.left, pos.x );
    SetHeight( m_boundingBox.top, pos.y );
}








