#ifndef ROCKEAST_HPP
#define ROCKEAST_HPP

#include "ProceduralObject.hpp"

class RockhouseEast : public ProceduralObject
{
    public:
    RockhouseEast();
    RockhouseEast( sf::Vector2f pos );
    virtual ~RockhouseEast() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
