#include "CoralGrass.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

GrassCoral::GrassCoral()
{
    m_type = "GrassCoral";
}

GrassCoral::GrassCoral( sf::Vector2f pos )
{
    m_type = "GrassCoral";
    Setup( pos );
}

void GrassCoral::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "GrassCoral::Setup" );
    m_maxFrame = 20;
    m_currentFrame = 0;
    m_animateSpeed = 1;
    BuildObject( pos, 0, sf::Color( 6, 96, 68 ) );
    ProceduralObject::Setup();
}

void GrassCoral::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    sf::Vector2f v1, v2, v3;
    int colorInc = 5;
    color.r = ( color.r + colorInc > 255 ) ? 255 : color.r + colorInc;
    color.g = ( color.g + colorInc > 255 ) ? 255 : color.g + colorInc;
    color.b = ( color.b + colorInc > 255 ) ? 255 : color.b + colorInc;

    float x = pos.x + chalo::GetRandomNumber( -10, 10 );
    float h = chalo::GetRandomNumber( 5, 20 );
    float w = chalo::GetRandomNumber( 5, 10 );

    if ( step == 10 )
    {
        return;
    }
    else if ( step % 3 == 0 )
    {
        v1 = sf::Vector2f( x, pos.y );
        v2 = sf::Vector2f( x + w, pos.y );
        v3 = sf::Vector2f( x + w, pos.y - h );
    }
    else if ( step % 3 == 1 )
    {
        v1 = sf::Vector2f( x, pos.y );
        v2 = sf::Vector2f( x + w, pos.y - h );
        v3 = sf::Vector2f( x + w, pos.y );
    }
    else if ( step % 3 == 2 )
    {
        v1 = sf::Vector2f( x, pos.y );
        v2 = sf::Vector2f( x + w, pos.y );
        v3 = sf::Vector2f( x + w / 2, pos.y - h );
    }

    AddVertex( 0, v1,     color );
    AddVertex( 0, v2,     color );
    AddVertex( 0, v3,     color );

    // Last index is the tip
    int tipIndex = m_vertexArrays[0].getVertexCount() - 1;
    m_vertexConnections[tipIndex].baseIndex = tipIndex;

    BuildObject( pos, step+1, color );
}


void GrassCoral::Update()
{
//    chalo::Logger::Out( "Update", "GrassCoral::Update" );
    IncrementFrame();

    if ( m_currentFrame == 0 )
    {
        for ( auto& v : m_vertexConnections )
        {
            float xChange = chalo::GetRandomNumber( -1, 1 );
            float yChange = chalo::GetRandomNumber( -1, 1 );

            AdjustVertex( 0, v.second.baseIndex, xChange, yChange );

            for ( auto& c : v.second.connectionIndices )
            {
                m_vertexArrays[0][c].position = m_vertexArrays[0][v.second.baseIndex].position;
            }
        }
    }
}


