#include "RockhouseWest.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

#include <cmath>

RockhouseWest::RockhouseWest()
{
    m_type = "RockhouseWest";
}

RockhouseWest::RockhouseWest( sf::Vector2f pos )
{
    m_type = "RockhouseWest";
    Setup( pos );
}

void RockhouseWest::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "RockhouseWest::Setup" );
    BuildObject( pos, 0, sf::Color( 50, 54, 65 ) );
    ProceduralObject::Setup();
}

void RockhouseWest::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float radius = 100;
    float height = 100;
    float quarteries = radius / 4;
    float halfsies = radius * sqrt( 2 ) / 2;

    // SHADOW
    sf::Color shadowColor = sf::Color( 25, 25, 25, 100 );
    sf::Vector2f center = sf::Vector2f( pos.x + 10, pos.y );
    m_vertexArrays[0].setPrimitiveType( sf::TriangleFan );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x,                  center.y ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + radius,         center.y ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + halfsies,       center.y + halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x,                  center.y + radius/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x - halfsies,       center.y + halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x - radius - 10,         center.y ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x - halfsies,       center.y - halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x,                  center.y - radius/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + halfsies-10,       center.y - halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + radius,         center.y ), shadowColor ) );


    // CAVE
    m_vertexArrays[1].setPrimitiveType( sf::TriangleFan );

    // Center vertex
    m_vertexArrays[1].append( sf::Vertex( pos, color ) );                                                           // a

    // Additional shape
    AddVertex( 1, sf::Vector2f( pos.x - radius,         pos.y ), color );                 // b
    AddVertex( 1, sf::Vector2f( pos.x - quarteries,     pos.y - height ), sf::Color( color.r+20, color.g+50, color.b+75 ) );        // c
    AddVertex( 1, sf::Vector2f( pos.x + quarteries,     pos.y - height ), sf::Color( color.r+20, color.g+50, color.b+75 ) );        // d
    AddVertex( 1, sf::Vector2f( pos.x + radius,         pos.y ), color );                 // e

    // Door
    sf::Color doorColor = sf::Color( 25, 25, 25 );
    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x - 80, pos.y ),   doorColor ) );
    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x - 40, pos.y - 60 ),   doorColor ) );
}
