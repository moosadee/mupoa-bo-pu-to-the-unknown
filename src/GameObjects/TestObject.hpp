#ifndef _TESTOBJECT_HPP
#define _TESTOBJECT_HPP

#include "ProceduralObject.hpp"

class TestObject : public ProceduralObject
{
    public:
    TestObject();
    TestObject( sf::Vector2f pos );
    virtual ~TestObject() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();
    virtual void Clear();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color, int parentIndex = -1 );
};

#endif
