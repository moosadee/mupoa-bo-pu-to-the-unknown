#include "Cephalopod.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

#include <cmath>

Cephalopod::Cephalopod()
{
    m_type = "Cephalopod";
}

Cephalopod::Cephalopod( sf::Vector2f pos )
{
    m_type = "Cephalopod";
    Setup( pos );
}

void Cephalopod::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "Cephalopod::Setup" );
    m_maxFrame = 4;
    m_currentFrame = 0;
    m_animateSpeed = 0.02;
    BuildObject( pos, 0, sf::Color( 175, 175, 175 ) );
    ProceduralObject::Setup();
}

void Cephalopod::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float w = chalo::GetRandomNumber( 15, 20 );
    float h = chalo::GetRandomNumber( 15, 20 );

    float radius = w;
    float quarteries = radius / 4;
    float halfsies = radius * sqrt( 2 ) / 2;

    // SHADOW
    sf::Color shadowColor = sf::Color( 25, 25, 25, 100 );
    sf::Vector2f center = sf::Vector2f( pos.x, pos.y+50 );
    m_vertexArrays[0].setPrimitiveType( sf::TriangleFan );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x,                  center.y ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + radius,         center.y ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + halfsies,       center.y + halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x,                  center.y + radius/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x - halfsies,       center.y + halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x - radius - 10,         center.y ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x - halfsies,       center.y - halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x,                  center.y - radius/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + halfsies-10,       center.y - halfsies/2 ), shadowColor ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( center.x + radius,         center.y ), shadowColor ) );


    // OBJECT

    // Body
    m_vertexArrays[1].setPrimitiveType( sf::Triangles );
    AddVertex( 1, sf::Vector2f( pos.x, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x - w, pos.y + h ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w, pos.y + h ), color );

    AddVertex( 1, sf::Vector2f( pos.x, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x - w, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x - w, pos.y + h ), color );

    AddVertex( 1, sf::Vector2f( pos.x, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w, pos.y + h ), color );

    AddVertex( 1, sf::Vector2f( pos.x, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x - w/2, pos.y - h ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w/2, pos.y - h ), color );

    AddVertex( 1, sf::Vector2f( pos.x, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x - w, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x - w/2, pos.y - h ), color );

    AddVertex( 1, sf::Vector2f( pos.x, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w, pos.y ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w/2, pos.y - h ), color );

    AddVertex( 1, sf::Vector2f( pos.x - w/2, pos.y - h ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w/2, pos.y - h  ), color );
    AddVertex( 1, sf::Vector2f( pos.x + w/4, pos.y - h*2 ), color );

    // Eyes
    float offset = 5;
    float ewidth = 4;
    float eheight = 10;

    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x - ewidth - offset, pos.y ), sf::Color::Black ) );
    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x + ewidth - offset, pos.y ), sf::Color::Black ) );
    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x + ewidth/2 - offset, pos.y - eheight ), sf::Color::Black ) );

    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x - ewidth + offset, pos.y ), sf::Color::Black ) );
    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x + ewidth + offset, pos.y ), sf::Color::Black ) );
    m_vertexArrays[1].append( sf::Vertex( sf::Vector2f( pos.x + ewidth/2 + offset, pos.y - eheight ), sf::Color::Black ) );

    // Tentacles
    float bottom = pos.y + h;
    float tentwidth = 4;
    for ( int x = pos.x - w; x < pos.x + w; x += tentwidth )
    {
        AddVertex( 1, sf::Vector2f( x, pos.y + h ), color );
        AddVertex( 1, sf::Vector2f( x + tentwidth, pos.y + h ), color );
        AddVertex( 1, sf::Vector2f( x + tentwidth/2, pos.y + h*2 ), color );

        int index = m_vertexArrays[1].getVertexCount() - 1;

        m_tentacleIndices.push_back( index );
    }
}


void Cephalopod::Update()
{
//    chalo::Logger::Out( "Update", "Cephalopod::Update" );
    IncrementFrame();

    if ( m_currentFrame < m_maxFrame / 2 )
    {
        for ( int i = 0; i < m_vertexArrays[1].getVertexCount(); i++ )
        {
            m_vertexArrays[1][i].position.y += (m_animateSpeed * 5);
        }

        for ( int i = 0; i < m_tentacleIndices.size() / 2; i++ )
        {
            int index = m_tentacleIndices[i];
            m_vertexArrays[1][index].position.x -= (m_animateSpeed * 2);
        }

        for ( int i = m_tentacleIndices.size() / 2; i < m_tentacleIndices.size(); i++ )
        {
            int index = m_tentacleIndices[i];
            m_vertexArrays[1][index].position.x += (m_animateSpeed * 2);
        }
    }
    else
    {
        for ( int i = 0; i < m_vertexArrays[1].getVertexCount(); i++ )
        {
            m_vertexArrays[1][i].position.y -= (m_animateSpeed * 5);
        }

        for ( int i = 0; i < m_tentacleIndices.size() / 2; i++ )
        {
            int index = m_tentacleIndices[i];
            m_vertexArrays[1][index].position.x += (m_animateSpeed * 2);
        }

        for ( int i = m_tentacleIndices.size() / 2; i < m_tentacleIndices.size(); i++ )
        {
            int index = m_tentacleIndices[i];
            m_vertexArrays[1][index].position.x -= (m_animateSpeed * 2);
        }
    }


}


