#include "TestObject.hpp"

#include "../chalo-engine/Utilities/Logger.hpp"
#include "../chalo-engine/Utilities/StringUtil.hpp"
#include "../chalo-engine/Utilities/HelperFunctions.hpp"

TestObject::TestObject()
{
    m_type = "TestObject";
}

TestObject::TestObject( sf::Vector2f pos )
{
    m_type = "TestObject";
    Setup( pos );
}

void TestObject::Setup( sf::Vector2f pos )
{
    ProceduralObject::Setup();
    m_maxFrame = 20;
    m_currentFrame = 0;
    m_animateSpeed = 1;
    BuildObject( pos, 0, sf::Color( 90, 0, 50 ), -1 );
    chalo::Logger::OutHighlight( "Setup done", "TestObject::Setup" );
}

void TestObject::BuildObject( sf::Vector2f pos, int step, sf::Color color, int parentIndex )
{
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x, pos.y ),          color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + 100, pos.y ),    color ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x, pos.y + 100 ),    color ) );


    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x, pos.y + 100  ),          sf::Color( 255, 0, 0 ) ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + 200, pos.y+100 ),       sf::Color( 255, 0, 0 ) ) );
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x, pos.y + 200 ),           sf::Color( 255, 0, 0 ) ) );

    m_vertexConnections[2].baseIndex = 2;
    m_vertexConnections[2].connectionIndices.push_back( 3 );
}


void TestObject::Update()
{
    IncrementFrame();

//    m_vertexArrays[0][2].position.x += 1;
    for ( auto& v : m_vertexConnections )
    {
        m_vertexArrays[0][v.second.baseIndex].position.x += 1;

        for ( auto& c : v.second.connectionIndices )
        {
            m_vertexArrays[0][c].position = m_vertexArrays[0][v.second.baseIndex].position;
        }
    }
}


void TestObject::Clear()
{
    ProceduralObject::Clear();
}
