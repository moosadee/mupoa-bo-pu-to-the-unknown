#ifndef _SPIKOCORAL_HPP
#define _SPIKOCORAL_HPP

#include "ProceduralObject.hpp"

class SpikoCoral : public ProceduralObject
{
    public:
    SpikoCoral();
    SpikoCoral( sf::Vector2f pos );
    virtual ~SpikoCoral() { ; }
    virtual void Setup( sf::Vector2f pos );
    virtual void Update();

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
