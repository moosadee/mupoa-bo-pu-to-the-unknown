#include "CoralFluff.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

FluffCoral::FluffCoral()
{
    m_type = "FluffCoral";
}

FluffCoral::FluffCoral( sf::Vector2f pos )
{
    m_type = "FluffCoral";
    Setup( pos );
}

void FluffCoral::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "FluffCoral::Setup" );
    m_maxFrame = 20;
    m_currentFrame = 0;
    m_animateSpeed = 1;
    BuildObject( pos, 0, sf::Color( 7, 59, 14 ) );
    ProceduralObject::Setup();
}

void FluffCoral::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float width = 30;
    float height = 30;
    int colorInc = 5;
    color.r = ( color.r + colorInc > 255 ) ? 255 : color.r + colorInc;
    color.g = ( color.g + colorInc > 255 ) ? 255 : color.g + colorInc;
    color.b = ( color.b + colorInc > 255 ) ? 255 : color.b + colorInc;

    sf::Vector2f bottom;
    sf::Vector2f node2 = sf::Vector2f(
        pos.x + chalo::GetRandomNumber( -width, width ),
        pos.y + chalo::GetRandomNumber( -height/2, -height ) );
    sf::Vector2f node3 = sf::Vector2f(
        pos.x + chalo::GetRandomNumber( width/4, width/2 ),
        pos.y + chalo::GetRandomNumber( -height/4, -height/2) );

    // Terminating case: Step is max number
    if ( step == 20 )
    {
        return;
    }

    bottom = sf::Vector2f( pos.x, pos.y );

    AddVertex( 0, node2, color );
    AddVertex( 0, node3, color );
    AddVertex( 0, bottom, color );

    int lastIndex = m_vertexArrays[0].getVertexCount() - 1;
    m_vertexConnections[lastIndex-1].baseIndex = lastIndex-1;
    m_vertexConnections[lastIndex-2].baseIndex = lastIndex-2;

    BuildObject( bottom, step + 1, color );
}


void FluffCoral::Update()
{
//    chalo::Logger::Out( "Update", "FluffCoral::Update" );
    IncrementFrame();

    if ( m_currentFrame == 0 )
    {
        for ( auto& v : m_vertexConnections )
        {
            float xChange = chalo::GetRandomNumber( -1, 1 );
            float yChange = chalo::GetRandomNumber( -1, 1 );

            AdjustVertex( 0, v.second.baseIndex, xChange, yChange );

            for ( auto& c : v.second.connectionIndices )
            {
                m_vertexArrays[0][c].position = m_vertexArrays[0][v.second.baseIndex].position;
            }
        }
    }
}


