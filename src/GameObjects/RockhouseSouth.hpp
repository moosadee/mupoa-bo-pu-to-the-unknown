#ifndef ROCKSouth_HPP
#define ROCKSouth_HPP

#include "ProceduralObject.hpp"

class RockhouseSouth : public ProceduralObject
{
    public:
    RockhouseSouth();
    RockhouseSouth( sf::Vector2f pos );
    virtual ~RockhouseSouth() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
