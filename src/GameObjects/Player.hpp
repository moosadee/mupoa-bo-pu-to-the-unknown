#ifndef _PLAYER_HPP
#define _PLAYER_HPP

#include "Cephalopod.hpp"

class Player
{
    public:
    Player();
    Player( sf::Vector2f pos );
    ~Player();
    void Setup( sf::Vector2f pos );
    void Update();
    void Draw( sf::RenderWindow& window );

    private:
    Cephalopod m_obj;
    float m_slowSpeed;
    float m_fastSpeed;
};

#endif
