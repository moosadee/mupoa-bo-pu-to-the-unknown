#include "CoralSpiko.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

SpikoCoral::SpikoCoral()
{
    m_type = "SpikoCoral";
}

SpikoCoral::SpikoCoral( sf::Vector2f pos )
{
    m_type = "SpikoCoral";
    Setup( pos );
}

void SpikoCoral::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "SpikoCoral::Setup" );
    ProceduralObject::Setup();
    m_maxFrame = 20;
    m_currentFrame = 0;
    m_animateSpeed = 1;
    BuildObject( pos, 0, sf::Color( 68, 50, 3 ) );
}

void SpikoCoral::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float height = 30;
    float width = 30;
    sf::Vector2f v1, v2, v3;
    int colorInc = 5;
    color.r = ( color.r + colorInc > 255 ) ? 255 : color.r + colorInc;
    color.g = ( color.g + colorInc > 255 ) ? 255 : color.g + colorInc;
    color.b = ( color.b + colorInc > 255 ) ? 255 : color.b + colorInc;

    if ( step == 12 )
    {
        return;
    }
    else if ( step == 0 )
    {
        float w = chalo::GetRandomNumber( width/5, width/3 );
        v1 = sf::Vector2f( pos.x - w,      pos.y );
        v2 = sf::Vector2f( pos.x + w,      pos.y );
        v3 = sf::Vector2f( pos.x + w/2,    pos.y - height );
    }
    else if ( step % 2 == 1 )
    {
        float y1 = chalo::GetRandomNumber( - height / 2, - 1/4 * height );
        float w = chalo::GetRandomNumber( width/2, width );
        float h = chalo::GetRandomNumber( 2, 5 );
        v1 = sf::Vector2f( pos.x,          pos.y + y1 );
        v2 = sf::Vector2f( pos.x,          pos.y + y1 + h );
        v3 = sf::Vector2f( pos.x - w,      pos.y + y1 - chalo::GetRandomNumber( 2, 10 ) );
    }
    else if ( step % 2 == 0 )
    {
        float y1 = chalo::GetRandomNumber( - height / 2, - 1/4 * height );
        float w = chalo::GetRandomNumber( width/2, width );
        float h = chalo::GetRandomNumber( 2, 5 );
        v1 = sf::Vector2f( pos.x,          pos.y + y1 );
        v2 = sf::Vector2f( pos.x,          pos.y + y1 + h );
        v3 = sf::Vector2f( pos.x + w,      pos.y + y1 - chalo::GetRandomNumber( 2, 10 ) );
    }

    AddVertex( 0, v1,     color );
    AddVertex( 0, v2,     color );
    AddVertex( 0, v3,     color );

    if ( step != 0 )
    {
        int lastIndex = m_vertexArrays[0].getVertexCount() - 1;
        m_vertexConnections[lastIndex].baseIndex = lastIndex;
    }

    BuildObject( pos, step+1, color );
}


void SpikoCoral::Update()
{
//    chalo::Logger::Out( "Update", "SpikoCoral::Update" );
    IncrementFrame();

    if ( m_currentFrame == 0 )
    {
        for ( auto& v : m_vertexConnections )
        {
            float xChange = chalo::GetRandomNumber( -1, 1 );
            float yChange = chalo::GetRandomNumber( -1, 1 );

            AdjustVertex( 0, v.second.baseIndex, xChange, yChange );

            for ( auto& c : v.second.connectionIndices )
            {
                m_vertexArrays[0][c].position = m_vertexArrays[0][v.second.baseIndex].position;
            }
        }
    }
}


