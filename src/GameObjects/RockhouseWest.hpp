#ifndef ROCKWest_HPP
#define ROCKWest_HPP

#include "ProceduralObject.hpp"

class RockhouseWest : public ProceduralObject
{
    public:
    RockhouseWest();
    RockhouseWest( sf::Vector2f pos );
    virtual ~RockhouseWest() { ; }
    virtual void Setup( sf::Vector2f pos );

    protected:
    virtual void BuildObject( sf::Vector2f pos, int step, sf::Color color );
};

#endif
