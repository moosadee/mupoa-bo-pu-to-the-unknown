#ifndef _PROCEDURAL_OBJECT_HPP
#define _PROCEDURAL_OBJECT_HPP

#include "VertexConnection.hpp"

#include <SFML/Graphics.hpp>
#include <string>
#include <map>

class ProceduralObject
{
    public:
    ProceduralObject();
    ProceduralObject( sf::Vector2f pos ) ;
    ProceduralObject( sf::Vector2f pos, sf::Color color );
    virtual ~ProceduralObject() { ; }
    virtual void Setup();
    virtual void Draw( sf::RenderWindow& window );
    virtual void Clear();
    virtual void Update();
    virtual std::string GetType();

    sf::IntRect GetBoundingBox();
    void Move( float x, float y );

    protected:
    std::string m_type;
    std::map<int, sf::VertexArray> m_vertexArrays;
    std::map<int, sf::VertexArray> m_originalPositions;
    std::map<int, VertexConnection> m_vertexConnections;

    sf::IntRect m_boundingBox;
    void SetLeft( float value );
    void SetTop( float value );
    void SetWidth( float value );
    void SetHeight( float value );
    void SetWidth( float leftXPos, float rightXPos );
    void SetHeight( float topYPos, float bottomYPos );
    void UpdateBoundingBox( sf::Vector2f pos );
    float GetLeft();
    float GetRight();
    float GetWidth();
    float GetTop();
    float GetBottom();
    float GetHeight();

    float m_animateSpeed;
    float m_currentFrame;
    float m_maxFrame;
    void IncrementFrame();

    void AdjustVertex( int key, int index, int xAmt, int yAmt );

    void AddVertex( int arrayIndex, sf::Vector2f position, sf::Color color );
};

#endif
