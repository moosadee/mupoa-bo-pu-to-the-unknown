#include "RockhouseNorth.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

RockhouseNorth::RockhouseNorth()
{
    m_type = "RockhouseNorth";
}

RockhouseNorth::RockhouseNorth( sf::Vector2f pos )
{
    m_type = "RockhouseNorth";
    Setup( pos );
}

void RockhouseNorth::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "RockhouseNorth::Setup" );
    BuildObject( pos, 0, sf::Color( 50, 54, 65 ) );
    ProceduralObject::Setup();
}

void RockhouseNorth::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    float radius = 100;
    float height = 100;
    float quarteries = radius / 4;

    m_vertexArrays[0].setPrimitiveType( sf::TriangleFan );

    // Center vertex
    m_vertexArrays[0].append( sf::Vertex( pos, color ) );                                                           // a

    // Additional shape
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - radius,         pos.y ), color ) );                 // b
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x - quarteries,     pos.y - height ), color ) );        // c
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + quarteries,     pos.y - height ), color ) );        // d
    m_vertexArrays[0].append( sf::Vertex( sf::Vector2f( pos.x + radius,         pos.y ), color ) );                 // e
}
