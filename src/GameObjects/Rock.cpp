#include "Rock.hpp"

#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/Logger.hpp"

Rock::Rock()
{
    m_type = "Rock";
}

Rock::Rock( sf::Vector2f pos )
{
    m_type = "Rock";
    Setup( pos );
}

void Rock::Setup( sf::Vector2f pos )
{
//    chalo::Logger::Out( "Setup", "Rock::Setup" );
    BuildObject( pos, 0, sf::Color( 100, 100, 100 ) );
    ProceduralObject::Setup();
}

void Rock::BuildObject( sf::Vector2f pos, int step, sf::Color color )
{
    m_vertexArrays[0].setPrimitiveType( sf::Triangles );

    sf::Vector2f v1, v2, v3;

    float height = 30;
    float width = 30;

    if ( step == 4 )
    {
        return;
    }

    // sf::Vector2f GetRandomCoordinate( int minX, int minY, int maxX, int maxY )
    v1 = pos;
    v2 = sf::Vector2f(
        pos.x + chalo::GetRandomNumber( width/3, width ),
        pos.y
        );
    v3 = sf::Vector2f( ( pos.x + v2.x ) / 2, pos.y - height );

    AddVertex( 0, v1,     color );
    AddVertex( 0, v2,     color );
    AddVertex( 0, v3,     color );

    sf::Vector2f refpoint = sf::Vector2f( ( pos.x + v2.x ) / 2, v2.y );

    BuildObject( refpoint, step+1, color );
}


