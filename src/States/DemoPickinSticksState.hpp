#ifndef _PICKINSTICKS
#define _PICKINSTICKS

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/UI/UILabel.hpp"

#include <vector>

class PickinSticksState : public chalo::IState
{
public:
    PickinSticksState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    std::vector<sf::Sprite> m_backgroundTiles;
    sf::Sprite m_player;
    float m_playerSpeed;
    sf::Sprite m_stick;

    chalo::UILabel m_scoreLabel;
    int m_score;

    void CreateMap();
};

#endif
