#include "TestState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/MathUtility.hpp"
#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Application/Application.hpp"
#include <cmath>

#include "../GameObjects/ProceduralObject.hpp"
#include "../GameObjects/Cephalopod.hpp"
#include "../GameObjects/CoralFluff.hpp"
#include "../GameObjects/CoralGrass.hpp"
#include "../GameObjects/CoralSpiko.hpp"
#include "../GameObjects/CoralTree.hpp"
#include "../GameObjects/Jellyfish.hpp"
#include "../GameObjects/Rock.hpp"
#include "../GameObjects/RockhouseEast.hpp"
#include "../GameObjects/RockhouseWest.hpp"
#include "../GameObjects/RockhouseNorth.hpp"
#include "../GameObjects/RockhouseSouth.hpp"
#include "../GameObjects/Geyser.hpp"
#include "../GameObjects/Splotch.hpp"
#include "../GameObjects/WaterReflection.hpp"
#include "../GameObjects/TestObject.hpp"

TestState::TestState()
{
}

TestState::~TestState()
{
    for ( auto& obj : m_objects )
    {
        if ( obj != nullptr )
        {
            delete obj;
        }
    }
}

void TestState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "TestState::Init", "function-trace" );
    IState::Init( name );
}

void TestState::Setup()
{
    chalo::Logger::Out( "", "TestState::Setup", "function-trace" );
    IState::Setup();

    chalo::InputManager::Setup();

    m_bg.setPosition( 0, 0 );
    m_bg.setSize( sf::Vector2f( 1280, 720 ) );
    m_bg.setFillColor( sf::Color( 74, 94, 158 ) );

    m_player.Setup( sf::Vector2f( 400, 400 ) );

    CreateObjects();
}

void TestState::Cleanup()
{
    chalo::Logger::Out( "", "TestState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
}

void TestState::Update()
{
//    if ( sf::Mouse::isButtonPressed( sf::Mouse::Left ) )
//    {
//        CreateObjects();
//    }

    for ( auto& obj : m_objects )
    {
        obj->Update();
    }

    m_player.Update();
}

void TestState::CreateObjects()
{
    m_objects.clear();

    // Place ground splotches
    for ( int i = 0; i < 10; i++ )
    {
        m_objects.push_back( new Splotch( chalo::GetRandomCoordinate( 0, 0, 1280, 720 ), sf::Color( 32, 53, 120, 50 ) ) );
        m_objects.push_back( new Splotch( chalo::GetRandomCoordinate( 0, 0, 1280, 720 ), sf::Color( 32, 53, 120, 50 ) ) );
    }

    // Place some Fluff Coral
    for ( int i = 0; i < 50; i++ )
    {
        m_objects.push_back( new FluffCoral( chalo::GetRandomCoordinate( 0, 0, 500, 720 ) ) );
        m_objects.push_back( new FluffCoral( chalo::GetRandomCoordinate( 1280-500, 0, 1280, 720 ) ) );
    }
    for ( int i = 0; i < 10; i++ )
    {
        m_objects.push_back( new FluffCoral( chalo::GetRandomCoordinate( 1280/2-200, 0, 1280/2+200, 720/2 ) ) );
        m_objects.push_back( new FluffCoral( chalo::GetRandomCoordinate( 1280/2-200, 720/2, 1280/2+200, 720 ) ) );
    }

    // Place some Spiko Coral
    for ( int i = 0; i < 3; i++ )
    {
        m_objects.push_back( new SpikoCoral( chalo::GetRandomCoordinate( 0, 0, 50, 720 ) ) );
        m_objects.push_back( new SpikoCoral( chalo::GetRandomCoordinate( 1280-50, 0, 1280, 720 ) ) );
    }

    // Place some Grass Coral
    for ( int i = 0; i < 50; i++ )
    {
        m_objects.push_back( new GrassCoral( chalo::GetRandomCoordinate( 1280/2-200, 720/2-100, 1280/2+200, 720/2+100 ) ) );
    }

    // Place some Tree Coral
    for ( int i = 0; i < 10; i++ )
    {
        m_objects.push_back( new TreeCoral( chalo::GetRandomCoordinate( 0, 0, 200, 720 ) ) );
        m_objects.push_back( new TreeCoral( chalo::GetRandomCoordinate( 1280-200, 0, 1280, 720 ) ) );
    }

    m_objects.push_back( new Geyser( sf::Vector2f( 1280/2, 720/2 ) ) );

    m_objects.push_back( new RockhouseEast( sf::Vector2f( 300, 100 ) ) );
    m_objects.push_back( new RockhouseEast( sf::Vector2f( 100, 400 ) ) );
    m_objects.push_back( new RockhouseEast( sf::Vector2f( 300, 700 ) ) );

    m_objects.push_back( new RockhouseWest( sf::Vector2f( 1280-300, 100 ) ) );
    m_objects.push_back( new RockhouseWest( sf::Vector2f( 1280-100, 400 ) ) );
    m_objects.push_back( new RockhouseWest( sf::Vector2f( 1280-300, 700 ) ) );

    m_objects.push_back( new Cephalopod( sf::Vector2f( 350, 100 ) ) );
    m_objects.push_back( new Cephalopod( sf::Vector2f( 250, 400 ) ) );
    m_objects.push_back( new Cephalopod( sf::Vector2f( 1280-300-100, 100 ) ) );
    m_objects.push_back( new Cephalopod( sf::Vector2f( 1280/2 - 100, 720/2 + 100 ) ) );

    m_objects.push_back( new FluffCoral( sf::Vector2f( 200, 500 ) ) );
    m_objects.push_back( new GrassCoral( sf::Vector2f( 300, 500 ) ) );
//    m_objects.push_back( new SpikoCoral( sf::Vector2f( 400, 500 ) ) );
//    m_objects.push_back( new TreeCoral( sf::Vector2f( 500, 500 ) ) );
//    m_objects.push_back( new Jellyfish( sf::Vector2f( 600, 500 ) ) );
//    m_objects.push_back( new Rock( sf::Vector2f( 700, 500 ) ) );


    // Water reflection last
    m_objects.push_back( new WaterReflection( sf::Vector2f( 0, 0 ) ) );
}

void TestState::Draw( sf::RenderWindow& window )
{
    window.draw( m_bg );

    for ( auto& obj : m_objects )
    {
        obj->Draw( window );
    }

    m_player.Draw( window );
}



