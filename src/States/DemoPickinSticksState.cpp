#include "DemoPickinSticksState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/HelperFunctions.hpp"
#include "../chalo-engine/Utilities/CollisionUtility.hpp"
#include "../chalo-engine/Application/Application.hpp"

PickinSticksState::PickinSticksState()
{
}

void PickinSticksState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "PickinSticksState::Init", "function-trace" );
    IState::Init( name );
}

void PickinSticksState::Setup()
{
    chalo::Logger::Out( "", "PickinSticksState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "tileset", "Content/Graphics/Demos/block.png" );
    chalo::TextureManager::Add( "moose",   "Content/Graphics/Demos/moose.png" );
    chalo::TextureManager::Add( "stick",   "Content/Graphics/Demos/stick.png" );

    chalo::InputManager::Setup();

    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();

    m_player.setTexture( chalo::TextureManager::Get( "moose" ) );
    m_player.setPosition( chalo::GetRandomCoordinate( 0, 0, screenWidth-32, screenHeight-32 ) );
    m_playerSpeed = 5;
    m_stick.setTexture( chalo::TextureManager::Get( "stick" ) );
    m_stick.setPosition( chalo::GetRandomCoordinate( 0, 0, screenWidth-32, screenHeight-32 ) );

    m_score = 0;
    m_scoreLabel.Setup(
        "score",
        "main",
        30,
        sf::Color::White,
        sf::Vector2f( 0, 0 ),
        "Score: " + chalo::StringUtility::IntegerToString( m_score ) );

    CreateMap();
}

void PickinSticksState::Cleanup()
{
    chalo::Logger::Out( "", "PickinSticksState::Cleanup", "function-trace" );
}

void PickinSticksState::CreateMap()
{
    int tileWH = 32;
    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();

    for ( int y = 0; y < screenHeight / tileWH + 1; y++ )
    {
        for ( int x = 0; x < screenWidth / tileWH; x++ )
        {
            sf::Sprite tile;
            tile.setTexture( chalo::TextureManager::Get( "tileset" ) );
            tile.setPosition( x * tileWH, y * tileWH );
            m_backgroundTiles.push_back( tile );
        }
    }
}

void PickinSticksState::Update()
{
    chalo::InputManager::Update();

    if ( chalo::InputManager::IsKeyPressed( sf::Keyboard::Escape ) || chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION6 ) ) )
    {
        SetGotoState( "startupstate" );
    }

    // Player movement
    sf::Vector2f pos = m_player.getPosition();
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_WEST ) ) )
    {
        pos.x -= m_playerSpeed;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_EAST ) ) )
    {
        pos.x += m_playerSpeed;
    }
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_NORTH ) ) )
    {
        pos.y -= m_playerSpeed;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_SOUTH ) ) )
    {
        pos.y += m_playerSpeed;
    }

    // Restrict to game screen
    int screenWidth = chalo::Application::GetScreenWidth();
    int screenHeight = chalo::Application::GetScreenHeight();
    pos = chalo::RestrictArea( pos, 0, 0, screenWidth-32, screenHeight-32 );

    m_player.setPosition( pos );

    // Check if collision
    if ( chalo::BoundingBoxCollision(
        m_player.getPosition().x, m_player.getPosition().y, 32, 32,
        m_stick.getPosition().x, m_stick.getPosition().y, 32, 32 ) )
    {
        m_score++;

        m_scoreLabel.SetText( "Score: " + chalo::StringUtility::IntegerToString( m_score ) );

        m_stick.setPosition( chalo::GetRandomCoordinate( 0, 0, screenWidth-32, screenHeight-32 ) );
    }
}

void PickinSticksState::Draw( sf::RenderWindow& window )
{
    // Draw background
    for ( auto& tile : m_backgroundTiles )
    {
        chalo::Application::GetWindow().draw( tile );
//        chalo::DrawManager::AddSprite( tile );
    }

    chalo::Application::GetWindow().draw( m_stick );
    chalo::Application::GetWindow().draw( m_player );
    chalo::Application::GetWindow().draw( m_scoreLabel.GetText() );
//    chalo::DrawManager::AddSprite( m_stick );
//    chalo::DrawManager::AddSprite( m_player );
//    chalo::DrawManager::AddUILabel( m_score );
}



