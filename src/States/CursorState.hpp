#ifndef _CURSOR_STATE
#define _CURSOR_STATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"

#include <string>

class CursorState : public chalo::IState
{
    public:
    CursorState();

    virtual void Init( const std::string& name, bool useCursors = false );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

    virtual void SetGotoState( const std::string& stateName );

    protected:
    void BackButtonHandler();
    int m_backTimer;
    sf::Sprite m_adminBlep;
    std::string m_backState;

    int m_logTimer;

    void CursorSetup();
    void CursorUpdate();
    void CursorDraw();
    std::vector<chalo::Character> m_cursors;
    bool m_useCursors;
    int m_cursorCount;
};


#endif
