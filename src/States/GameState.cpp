#include "GameState.hpp"

#include "../chalo-engine/Managers/MenuManager.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Managers/ConfigManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"
#include "../chalo-engine/Utilities/MathUtility.hpp"
#include "../chalo-engine/Application/Application.hpp"
#include <cmath>

GameState::GameState()
{
}

void GameState::Init( const std::string& name )
{
    chalo::Logger::Out( "Parameters - name: " + name, "GameState::Init", "function-trace" );
    IState::Init( name );
}

void GameState::Setup()
{
    chalo::Logger::Out( "", "GameState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "tileset", "Content/Graphics/Game/tiles.png" );
    chalo::TextureManager::Add( "objects", "Content/Graphics/Game/items.png" );
    chalo::TextureManager::Add( "character", "Content/Graphics/Game/body.png" );
    chalo::TextureManager::Add( "tentacle", "Content/Graphics/Game/tentacle.png" );

    chalo::InputManager::Setup();

    m_line.setPosition( 0, 0 );

    int playerX = 1280/2;
    int playerY = 720/2;

    m_character.setTexture( chalo::TextureManager::Get( "character" ) );
    m_character.setPosition( playerX, playerY );
    m_playerSpeed = 5;

    for ( int i = 0; i < 8; i++ )
//    for ( int i = 0; i < 1; i++ )
    {
        sf::Sprite tentacle;
        tentacle.setTexture( chalo::TextureManager::Get( "tentacle" ) );
        int xOff = i * 3;
        int yOff = 32;
        int angle = rand() % 90 - 45;
        tentacle.setOrigin( 12, 0 );
        tentacle.setRotation( angle );
        tentacle.setPosition( playerX + xOff, playerY + yOff );

        m_tentacles.push_back( tentacle );
    }

    int tileWH = 32;
    m_mapWidth = 40;
    m_mapHeight = 23;

    // sf::Sprite m_map[80][45];
    for ( int y = 0; y < m_mapHeight; y++ )
    {
        for ( int x = 0; x < m_mapWidth; x++ )
        {
            m_map[x][y].setTexture( chalo::TextureManager::Get( "tileset" ) );
            m_map[x][y].setTextureRect( sf::IntRect( 0, 0, tileWH, tileWH ) );
            m_map[x][y].setPosition( x * tileWH, y * tileWH );
        }
    }

    int randObjects = rand() % 50 + 50;
    for ( int i = 0; i < randObjects; i++ )
    {
        int object = rand() % 10;
        int randX = rand() % 1280;
        int randY = rand() % 720;

        sf::Sprite sprObj;
        sprObj.setTexture( chalo::TextureManager::Get( "objects" ) );
        sprObj.setPosition( randX, randY );
        sprObj.setTextureRect( sf::IntRect( object * tileWH, 0, tileWH, tileWH ) );
        m_objects.push_back( sprObj );
    }
}

void GameState::Cleanup()
{
    chalo::Logger::Out( "", "GameState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
}

void GameState::Update()
{
    bool playerMoving = false;

    sf::Vector2f pos = m_character.getPosition();
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_WEST ) ) )
    {
        pos.x -= m_playerSpeed;

        // Move tentacles
//        for ( unsigned int i = 0; i < m_tentacles.size(); i++ )
//        {
//            float angle = m_tentacles[i].getRotation();
//            if ( angle >= 315 ) { angle -= 360; }
//            angle -= rand() % 3;
//
//            if      ( angle < -45 ) { angle = -45; angle += 360; }
//            else if ( angle > 45 ) { angle = 45; }
//
//            m_tentacles[i].setRotation( angle );
//        }
        playerMoving = true;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_EAST ) ) )
    {
        pos.x += m_playerSpeed;

        // Move tentacles
//        for ( unsigned int i = 0; i < m_tentacles.size(); i++ )
//        {
//            float angle = m_tentacles[i].getRotation();
//            if ( angle >= 315 ) { angle -= 360; }
//            angle += rand() % 3;
//
//            if      ( angle < -45 ) { angle = -45; angle += 360; }
//            else if ( angle > 45 ) { angle = 45; }
//
//            m_tentacles[i].setRotation( angle );
//        }
        playerMoving = true;
    }
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_NORTH ) ) )
    {
        pos.y -= m_playerSpeed;
        playerMoving = true;
    }
    else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_SOUTH ) ) )
    {
        pos.y += m_playerSpeed;
        playerMoving = true;
    }

    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION1 ) ) )
    {
        float angle = m_tentacles[0].getRotation();
        angle+=1;
        m_tentacles[0].setRotation(angle);
        chalo::Logger::Out( "Tentacle 0 angle: " + chalo::StringUtility::FloatToString( angle ), "GameState::Update" );
    }
    else if (chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION2 ) ) )
    {
        float angle = m_tentacles[0].getRotation();
        angle-=1;
        m_tentacles[0].setRotation(angle);
        chalo::Logger::Out( "Tentacle 0 angle: " + chalo::StringUtility::FloatToString( angle ), "GameState::Update" );
    }
//
//    if ( !playerMoving )
//    {
//        // Move tentacles
//        for ( unsigned int i = 0; i < m_tentacles.size(); i++ )
//        {
//            float angle = m_tentacles[i].getRotation();
//            if ( angle >= 315 ) { angle -= 360; }
//
//            int random = rand() % 3 - 1;    // 0 1 2    // -1 0 1
//            angle += random;
//
//            if      ( angle < -45 ) { angle = -45; angle += 360; }
//            else if ( angle > 45 ) { angle = 45; }
//
//            m_tentacles[i].setRotation( angle );
//        }
//    }

    m_character.setPosition( pos );

    for ( unsigned int i = 0; i < m_tentacles.size(); i++ )
    {
        int xOff = i * 3;
        int yOff = 32;
        m_tentacles[i].setPosition( pos.x + xOff, pos.y + yOff );


        // Angle: 45 to 0, 360 to 315
        //int angle = rand() % 180 - 90;

//        float angle = m_tentacles[i].getRotation();
//
//        if ( angle >= 315 ) { angle -= 360; }
//
//        chalo::Logger::Out( "Random " + chalo::StringUtility::IntegerToString( random ), "GameState::Update" );
//
//        int random = rand() % 3 - 1;    // 0 1 2    // -1 0 1
//        angle += random;
//
//        if ( i == 0 )
//            chalo::Logger::Out( "Tentacle " + chalo::StringUtility::IntegerToString( i ) + " angle: " + chalo::StringUtility::FloatToString( angle ), "GameState::Update" );
//
//        // Keep tentacle between -45 and 45 degrees
//        if      ( angle < -45 ) { angle = -45; angle += 360; }
//        else if ( angle > 45 ) { angle = 45; }
//
//        m_tentacles[i].setRotation( angle );
    }
}

void GameState::Draw( sf::RenderWindow& window )
{
    for ( int y = 0; y < m_mapHeight; y++ )
    {
        for ( int x = 0; x < m_mapWidth; x++ )
        {
            window.draw( m_map[x][y] );
        }
    }

    for ( auto& obj : m_objects )
    {
        window.draw( obj );
    }

    for ( auto& tentacle : m_tentacles )
    {
        window.draw( tentacle );
    }
    window.draw( m_character );

    sf::Vector2i mousePos = chalo::InputManager::GetMousePosition();
    sf::Vector2f tentPos = m_tentacles[0].getPosition();

    sf::Vertex line[] = {
        sf::Vertex( sf::Vector2f( mousePos.x, mousePos.y ) ),
        sf::Vertex( tentPos )
    };

    window.draw( line, 2, sf::Lines );
}



