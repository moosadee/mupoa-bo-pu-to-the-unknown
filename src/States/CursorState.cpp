#include "CursorState.hpp"

#include "../chalo-engine/Application/Application.hpp"
#include "../chalo-engine/Managers/InputManager.hpp"
#include "../chalo-engine/Utilities/Messager.hpp"

CursorState::CursorState()
{
}

void CursorState::Init( const std::string& name, bool useCursors )
{
    chalo::Logger::Out( "Parameters - name: " + name, "CursorState::Init", "function-trace" );
    IState::Init( name );
    m_useCursors = useCursors;
    m_cursorCount = 1;
}

void CursorState::Setup()
{
    chalo::Logger::Out( "", "CursorState::Setup", "function-trace" );
    IState::Setup();

    chalo::TextureManager::Add( "adminFlags", "Content/Graphics/UI/admin-flags.png" );

    if ( m_useCursors )
    {
        chalo::TextureManager::Add( "cursor1",  "Content/Graphics/UI/cursor1.png" );
        CursorSetup();
    }

    // Visual flag
    m_backTimer = 0;
    m_adminBlep.setTexture( chalo::TextureManager::Get( "adminFlags" ) );

    m_logTimer = 0;
}

void CursorState::Cleanup()
{
    chalo::Logger::Out( "", "CursorState::Cleanup", "function-trace" );
    chalo::MenuManager::Cleanup();
    chalo::DrawManager::Reset();
    chalo::TextureManager::Clear();
    m_cursors.clear();
}

void CursorState::Update()
{
    m_logTimer++;
    if ( m_logTimer > 500 )
    {
        chalo::Logger::Out( "", "CursorState::Update", "function-trace" );
        m_logTimer = 0;
    }

    if ( m_useCursors )
    {
        CursorUpdate();
    }

    chalo::InputManager::Update();
    chalo::MenuManager::Update();
}

void CursorState::Draw( sf::RenderWindow& window )
{
    if ( m_useCursors )
    {
        CursorDraw();
    }
}

void CursorState::BackButtonHandler()
{
    // Check for back command
    if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( 0, chalo::INPUT_ACTION6 ) ) )
    {
        chalo::Logger::Out( "Holding back... " + chalo::StringUtility::IntegerToString( m_backTimer ), "CursorState::BackButtonHandler", "function-trace" );
        chalo::DrawManager::AddSprite( m_adminBlep );
        m_backTimer++;
    }
    else
    {
        m_backTimer = 0;
    }

    if ( m_backTimer >= 100 )
    {
        SetGotoState( m_backState );
    }
}

void CursorState::CursorSetup()
{
    m_cursors.clear();
    sf::IntRect frame = sf::IntRect( 0, 0, 62, 62 );

    chalo::Character cursor1;
    cursor1.Setup();
    cursor1.SetTexture( chalo::TextureManager::Get( "cursor1" ), frame );

    if ( chalo::Messager::Has( "player1cursor_x" ) )
    {
        cursor1.SetPosition(
            chalo::Messager::GetInt( "player1cursor_x" ),
            chalo::Messager::GetInt( "player1cursor_y" )
        );
        cursor1.SetActiveState( chalo::ACTIVE );
    }
    else
    {
        int centerX = chalo::Application::GetScreenWidth() / 2 - 31;
        int centerY = chalo::Application::GetScreenHeight() / 2 - 31;
        cursor1.SetPosition( centerX, centerY );
        cursor1.SetActiveState( chalo::INVISIBLE );
    }

    cursor1.SetAnimationInformation( 1, 0.1 );
    m_cursors.push_back( cursor1 );
}

void CursorState::CursorUpdate()
{
    // Check inputs
    for ( int i = 0; i < m_cursorCount; i++ )
    {
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_WEST ) ) )
        {
            m_cursors[i].Move( chalo::WEST );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_EAST ) ) )
        {
            m_cursors[i].Move( chalo::EAST );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
        if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_NORTH ) ) )
        {
            m_cursors[i].Move( chalo::NORTH );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
        else if ( chalo::InputManager::IsActionActive( chalo::PlayerInputAction( i, chalo::INPUT_SOUTH ) ) )
        {
            m_cursors[i].Move( chalo::SOUTH );
            m_cursors[i].SetActiveState( chalo::ACTIVE );
        }
    }
}

void CursorState::CursorDraw()
{
    for ( auto& cursor : m_cursors )
    {
        if ( cursor.GetActiveState() == chalo::ACTIVE )
        {
            cursor.ForceSpriteUpdate();
            chalo::DrawManager::AddSprite( cursor.GetSprite() );
        }
    }
}

void CursorState::SetGotoState( const std::string& stateName )
{
    if ( m_cursors[0].GetActiveState() == chalo::ACTIVE )
    {
        chalo::Messager::Set( "player1cursor_x", chalo::StringUtility::IntegerToString( m_cursors[0].GetPosition().x ) );
        chalo::Messager::Set( "player1cursor_y", chalo::StringUtility::IntegerToString( m_cursors[0].GetPosition().y ) );
    }
    m_gotoState = stateName;
}
