#ifndef _GAMESTATE
#define _GAMESTATE

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../chalo-engine/GameObjects/Character.hpp"

#include <vector>

class GameState : public chalo::IState
{
public:
    GameState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    chalo::Character m_player;
    float m_playerSpeed;
    sf::Sprite m_map[40][23]; // TODO: TEMPORARY
    int m_mapWidth, m_mapHeight;
    std::vector<sf::Sprite> m_objects; // TODO: TEMPORARY
    std::vector<sf::Sprite> m_tentacles; // TODO: TEMPORARY
    sf::RectangleShape m_line;
    sf::Sprite m_character;
};

#endif
