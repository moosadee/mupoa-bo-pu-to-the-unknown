#ifndef _TestState_HPP
#define _TestState_HPP

#include "../chalo-engine/States/IState.hpp"
#include "../chalo-engine/GameObjects/GameObject.hpp"
#include "../chalo-engine/Maps/WritableMap.hpp"
#include "../chalo-engine/Managers/TextureManager.hpp"
#include "../chalo-engine/Managers/FontManager.hpp"
#include "../chalo-engine/Managers/DrawManager.hpp"
#include "../GameObjects/ProceduralObject.hpp"
#include "../GameObjects/Player.hpp"

#include <vector>

class TestState : public chalo::IState
{
public:
    TestState();
    ~TestState();

    virtual void Init( const std::string& name );
    virtual void Setup();
    virtual void Cleanup();
    virtual void Update();
    virtual void Draw( sf::RenderWindow& window );

private:
    void CreateObjects();
    sf::RectangleShape m_bg;
    Player m_player;
    std::vector<ProceduralObject*> m_objects;
};

#endif
